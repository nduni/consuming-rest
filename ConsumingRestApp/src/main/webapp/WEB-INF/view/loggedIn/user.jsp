<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<title>User Main Page</title>
	<script src="http://www.openlayers.org/api/OpenLayers.js"></script>
	
</head>
<body>
<h2>Jesteś zalogowany jako: Użytkownik</h2>
<br><br>
<div id="mapdiv" style="width: 400px; height: 400px;">
   <!-- tu będzie mapka -->
</div>
<script type="text/javascript">
window.onload = function() {
	map = new OpenLayers.Map("mapdiv");
	// dodajemy warstwę OpenStreetMap
	map.addLayer(new OpenLayers.Layer.OSM());
	// definiujemy punkt środka
	var lonLat = new OpenLayers.LonLat( 19.25252 ,52.06516 )
	   .transform(
	      new OpenLayers.Projection("EPSG:4326"),
	      map.getProjectionObject()
	   );
	// ustawiamy pozycję i zoom mapy
	map.setCenter (lonLat, 6);
	
	var markers = new OpenLayers.Layer.Markers("Markers");
	map.addLayer(markers);
	markers.addMarker(new OpenLayers.Marker(lonLat));

	var vectorLayer = new OpenLayers.Layer.Vector("Overlay");
	map.addLayer(vectorLayer);	
	var feature = new OpenLayers.Feature.Vector(
			   new OpenLayers.Geometry.Point( 19.25252 ,52.06516  ).
			      transform(
			         new OpenLayers.Projection("EPSG:4326"),
			         map.getProjectionObject()
			      ),
			      {description:''} ,
			      {
			         externalGraphic: 'http://openlayers.org/api/img/marker.png',
			         graphicHeight: 25,
			         graphicWidth: 21,
			         graphicXOffset:-12,
			         graphicYOffset:-25
			      }
			);
			vectorLayer.addFeatures(feature);
			// selektor grupujący funkcje
			var selector = new OpenLayers.Control.SelectFeature(vectorLayer, {
			   onSelect: createPopup,
			   onUnselect: destroyPopup
			});
			// tworzenie dymka
			function createPopup(feature) {
			   feature.popup = new OpenLayers.Popup.FramedCloud("pop",
			         feature.geometry.getBounds().getCenterLonLat(),
			         null,
			         '<div class="markerContent">'+feature.attributes.description+'</div>',
			         null,
			         true,
			         function() { selector.unselectAll(); }
			   );
			   //feature.popup.closeOnMove = true;
			   map.addPopup(feature.popup);
			}
			// niszczenie dymka
			function destroyPopup(feature) {
			   feature.popup.destroy();
			   feature.popup = null;
			}
			map.addControl(selector);
			selector.activate();
	
}
</script>
<br><br>
<form:form action="${pageContext.request.contextPath }/logout" method="POST">
<input type="submit" value="WYLOGUJ"/>
</form:form>
</body>
</html>