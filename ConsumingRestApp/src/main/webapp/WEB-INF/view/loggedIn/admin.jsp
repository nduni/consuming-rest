<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Admin Main Page</title>

</head>
<body>
	<h2>Jesteś zalogoway jako: Admin</h2>
	<form:form action="${pageContext.request.contextPath }/logout"
		method="POST">
		<input type="submit" value="WYLOGUJ" />
	</form:form>

	<p>Jako Admin masz uprawnienia do zmiany widoku dla użytkownika
		"Gość" oraz przyznawać dodatkowe widoki dla zarejestrowanych
		użytkowników.</p>


	<p>Ustaw stację dla zalogowany jako gość:</p>
	<form action="${pageContext.request.contextPath }/api/guestStation" method="POST" >
		<select name="station">
			<c:forEach items="${stations}" var="station">
				<option value="${station.id}">${station.stationName}</option>
			</c:forEach>
		</select>
		<input type="submit" value="Zapisz" />
	</form>
	<p>
		Ustaw stacje do wyboru dla zarejestrowanego użytkownika:
		<!-- combobox z multi wyborem stacji -->
		<form:form
			action="${pageContext.request.contextPath }/userStations"
			method="POST">
			<select name="station" multiple style="max-width: 50%; height: 50%">
				<c:forEach items="${stations}" var="station">
					<option value="${station}">${station.stationName}</option>
				</c:forEach>
			</select>
			<input type="submit" value="Zapisz" />
		</form:form>
</body>
</html>