<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<meta charset="utf-8">
	<script src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.2.0/build/ol.js"></script>
	<title>Guest Main Page</title>
</head>
<body>
<h2>Jesteś zalogowany jako Gość</h2>
<div id="mapa" style="width: 60%; height: 400px;">
   
</div>
<script type="text/javascript">
var map = new ol.Map({
    target: 'mapa',
    layers: [
      new ol.layer.Tile({
        source: new ol.source.OSM()
      })
    ],
    view: new ol.View({
      center: ol.proj.fromLonLat([19, 52]),
      zoom: 6
    })
  });
</script>
<form:form action="${pageContext.request.contextPath }/logout" method="POST">
<input type="submit" value="WYLOGUJ"/>
</form:form>
</body>
</html>