<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<title>Pomiar jakości powietrza</title>
	<style>
	.failed {
		color: red;
			}
	</style>
	</head>
<body>
	<h2>Pomiar jakości powietrza - Strona główna</h2>

	<form:form action="${pageContext.request.contextPath}/logginProcess/authenticateTheUser"
			   method="POST">
		
		<c:if test="${param.error != null}">
		
			<i class="failed">Zła nazwa użytkownika lub hasło</i>
			
		</c:if>
			
		<p>
			Login: <input type="text" name="username" value="guest"/>
		</p>

		<p>
			Hasło: <input type="password" name="password" value="guest"/>
		</p>
		
		<input type="submit" value="Zaloguj" />
		
	</form:form>
	<p>
	<a href="${pageContext.request.contextPath}/">Porwrót do strony głównej</a>
	</p>

</body>
</html>