<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<title>Pomiar jakości powietrza</title>
</head>
<body>
	<h2>Pomiar jakości powietrza - Strona główna</h2>
	<p>
			  
	Witaj na stronie głównej. 
	
	Przechodząc do strony z logowaniem możesz użyć logowania jako gość - wystarczy, że pozostawisz wypełniony login: "guest" oraz hasło i klikniesz przycisk zaloguj.
	Użytkownik zalogowany jako gość może sprawdzać pomiar jakości powietrza tylko dla stacji wybranej przez Administratora (jest to wersja demo serwisu).
	
	Jeśli jesteś zarejestrowanym użytkownikiem wpisz własny login oraz hasło.
	
	<p>
	<a href="${pageContext.request.contextPath}/logginProcess/login-page">Przejd&#378; do strony logowania</a>
	</p>
	
</body>
</html>