<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
	<title>Pomiar jakości powietrza - Brak dostępu </title>
</head>
<body>
	<h2>Brak dostępu - nie masz uprawnień do przeglądania tej strony</h2>
	<hr>	
	<a href="${pageContext.request.contextPath}/">Powrót do strony głównej</a>	
</body>
</html>