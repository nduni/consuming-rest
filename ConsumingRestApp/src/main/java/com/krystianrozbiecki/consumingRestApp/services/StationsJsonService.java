package com.krystianrozbiecki.consumingRestApp.services;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.krystianrozbiecki.consumingRestApp.domain.Station;

@Service("jsonStationsReader")
public class StationsJsonService {
	
	private List<Station> stations;

	public StationsJsonService() {
	
	RestTemplate rest = new RestTemplate();
	final String url = "http://api.gios.gov.pl/pjp-api/rest/station/findAll"; 
	ResponseEntity<List<Station>> response = rest.exchange(
			  url,
			  HttpMethod.GET,
			  null,
			  new ParameterizedTypeReference<List<Station>>(){});
			stations = response.getBody();
			stations.sort((s1, s2) -> s1.getStationName().compareTo(s2.getStationName()));
	}


	public void setStations(List<Station> stations) {
		this.stations = stations;
	}

	public List<Station> getStations() {
		return stations;
	}

	@Override
	public String toString() {
		return "Wszystkie stacje pomiarowe ["+ stations + "]";
	}
	
}
