package com.krystianrozbiecki.consumingRestApp.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class Station {
	private long id;
	private String stationName;
	private double gegrLat;
	private double gegrLon;
	private City city;
	private String addressStreet;

	

	public Station() {

	}
	public void setId(long id) {
		this.id = id;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public void setGegrLat(double gegrLat) {
		this.gegrLat = gegrLat;
	}

	public void setGegrLon(double gegrLon) {
		this.gegrLon = gegrLon;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public void setAddressStreet(String addressStreet) {
		this.addressStreet = addressStreet;
	}
	public long getId() {
		return id;
	}

	public String getStationName() {
		return stationName;
	}

	public double getGegrLat() {
		return gegrLat;
	}
	public double getGegrLon() {
		return gegrLon;
	}
	public City getCity() {
		return city;
	}

	public String getAddressStreet() {
		return addressStreet;
	}

	@Override
	public String toString() {
		return "Stacja: " + stationName + ", ( " + gegrLat + "\u00B0"+"N"+", " + gegrLon
				+ "\u00B0"+"E), Miasto: " + city + ", Adres: " + addressStreet;
	}

}