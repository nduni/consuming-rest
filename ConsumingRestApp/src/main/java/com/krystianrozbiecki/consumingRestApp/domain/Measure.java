package com.krystianrozbiecki.consumingRestApp.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Measure {
	private String key;
	private Values[] values;
	
	public Measure() {
		
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Values[] getValues() {
		return values;
	}
	public void setValues(Values[] values) {
		this.values = values;
	}

	@Override
	public String toString() {
		return "Measure [key=" + key + ", values=" + values[0] + "]";
	}
	
}
