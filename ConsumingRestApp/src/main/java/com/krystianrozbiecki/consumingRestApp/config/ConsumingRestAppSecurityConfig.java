package com.krystianrozbiecki.consumingRestApp.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class ConsumingRestAppSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource securityDataSource;
	@Autowired
	private SimpleAuthenticationSuccessHandler successHandler;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.jdbcAuthentication().dataSource(securityDataSource);
		// .withUser(users.username("janek").password("1234").roles("ADMIN"))
		// .withUser(users.username("seba").password("registered").roles("GUEST"))
		// .withUser(users.username("tola").password("adminone").roles("USER"));
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
				// .antMatchers("/","/home/**").permitAll()
				.antMatchers("/loggedIn/guest").hasRole("GUEST")
				.antMatchers("/loggedIn/user").hasRole("USER")
				.antMatchers("/loggedIn/admin").hasRole("ADMIN")
				.anyRequest().authenticated().and().formLogin()
				.successHandler(successHandler).loginPage("/logginProcess/login-page")
				.loginProcessingUrl("/logginProcess/authenticateTheUser").permitAll().and().logout().permitAll().and()
				.exceptionHandling().accessDeniedPage("/home/access-denied")
				.and().csrf().disable();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/home/**", "/","/logginProcess/login-as-a-guest");
	}
}
