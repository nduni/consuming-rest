package com.krystianrozbiecki.consumingRestApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
	
	@GetMapping("/")
	public String showHome() {
		return "/home/home";
	}
	@GetMapping("/home/home")
	public String showHome2() {
		return "/home/home";
	}
	@GetMapping("/home/access-denied")
	public String showAccessDenied() {
		return "home/access-denied";
	}

}
