package com.krystianrozbiecki.consumingRestApp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.krystianrozbiecki.consumingRestApp.services.StationsJsonService;

@Controller
public class LoginController {
	
	@Autowired 
	private StationsJsonService jsonStationsReader;
	
	@GetMapping("/logginProcess/login-page")
	public String showLoginPage() {
		return "logginProcess/login-page";
	}

	@GetMapping("guest")
	public String showGuest() {
		return "loggedIn/guest";

	}

	@GetMapping("/user")
	public String showUser(Model model) {
		// add saved guestStation from DB
		return "loggedIn/user";

	}

	@GetMapping("/admin")
	public String showAdmin(Model model) {	

		model.addAttribute("stations", jsonStationsReader.getStations());
		return "loggedIn/admin";

	}
}
