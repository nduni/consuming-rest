package com.krystianrozbiecki.consumingRestApp.controller;

import java.util.Collections;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.krystianrozbiecki.consumingRestApp.domain.Station;

@RestController
@RequestMapping("/api")
public class StationController {

	@GetMapping("/findall")
	public List<Station> showAllStations() {
		RestTemplate rest = new RestTemplate();
		final String url = "http://api.gios.gov.pl/pjp-api/rest/station/findAll";
		ResponseEntity<List<Station>> response = rest.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Station>>() {
				});
		List<Station> stations = response.getBody();
		stations.sort((s1, s2) -> s1.getStationName().compareTo(s2.getStationName()));
		return stations;
	}

	@PostMapping("/guestStation")
	public void saveGuestStation(Station station) {
		System.out.println(station.toString());
	}

	@PostMapping("/userStations")
	public List<Station> saveUserStations(@RequestBody List<Station> stations) {
		// TO-DO add stations mapping
		return stations;
	}

}
